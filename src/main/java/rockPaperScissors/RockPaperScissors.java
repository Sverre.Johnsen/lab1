package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
        System.out.println("Bye bye :)");
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");


    private Boolean isWinner(String choice1, String choice2) {
        if (choice1.equals("paper")){
            return choice2.equals("rock");
        }
        else if (choice1.equals("scissors")) {
            return choice2.equals("paper");
        }


        else {
            return choice2.equals("scissors");
        }
    }

    private String userChoice() {
        while(true) {
           String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
           if (validateInput(humanChoice, rpsChoices)) {
               return humanChoice;
           }
           else {
               System.out.println("I don't understand" + humanChoice + ". Try again");
           }
        }
    }

    private boolean validateInput(String input, List<String> validInput) {
        input.toLowerCase();
        return validInput.contains(input);
    }

    private String continuePlaying() {
        while(true) {
            String continueAnswer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            List<String> yn = Arrays.asList("y", "n");
            if (validateInput(continueAnswer, yn)) {
                return continueAnswer;
            }
            else {
                System.out.println("I do not understand " + continueAnswer + ". Could you try again?");
            }
        }

    }

    private String randomChoice() {
        Random rand = new Random();

        String rps = rpsChoices.get(rand.nextInt(rpsChoices.size()));

        return rps;
    }

    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true) {
            System.out.println("Let's play round " + roundCounter);
            roundCounter++;
            String humanChoice = userChoice();

            String computerChoice = randomChoice();

            String choice = "Human chose " + humanChoice + ", computer chose " + computerChoice + ".";


            if (isWinner(humanChoice, computerChoice)) {
                System.out.println(choice + " Human wins!");
                humanScore++;
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);


            } else if (isWinner(computerChoice, humanChoice)) {
                System.out.println(choice + " Computer wins!");
                computerScore++;
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            } else {
                System.out.println(choice + " It's a tie!");
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            }




            if (continuePlaying().equals("n")){

                break;

            }





        }

    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
